FROM cloudron/base:0.8.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io> <support@cloudron.io>

ENV PATH /usr/local/node-4.2.1/bin:$PATH

RUN npm install -g tiddlywiki@5.1.9

ADD start.sh /app/code/start.sh

EXPOSE 8080

CMD [ "/app/code/start.sh" ]

