#!/bin/bash

set -eu -o pipefail

if [[ ! -f "/app/data/tiddlywiki.info" ]]; then
    # http://tiddlywiki.com/static/Installing%2520TiddlyWiki%2520on%2520Node.js.html
    echo "initializing wiki on first run"
    tiddlywiki /app/data --init server
fi

chown -R cloudron:cloudron /app/data

# http://tiddlywiki.com/static/ServerCommand.html
echo "Starting server"
exec /usr/local/bin/gosu cloudron:cloudron tiddlywiki /app/data --server 8080 $:/core/save/all text/plain text/html user "" 0.0.0.0

